FolderStructure-

1) Selenium- All Selenium code is here
2) TestRail- All TestRail API interaction is here
3) TestNG Report- Test NG report of automated runs,status of automated run will be mapped in TestRail

More Details-

In Selenium package,in PageObjectAuctivaInstantiate class ,validateSuccessFullLogin method is runner method,it will do ui automation,create TestNG report,use the status of report and map into the
TestRail against the testcase

TestRail-

In TestRail,we have API's for testProjects,testsuite,testRun,testresult etc

how Selenium and testRail is linked - In Selenium package,in PageObjectAuctivaInstantiate class ,in validateSuccessFullLogin method,after automated execution and testNG report generation 
and parsing,there is method testResult.populateTestResult(projectName,suiteName,testName,testRunName),this method will populated automated run result in TestRail