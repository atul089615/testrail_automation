package Selenium;

import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.How;

public class PageObjectAuctiva {

    @FindBy(how = How.ID, using = "ftctrl_Header_btn_SignIn")
    private WebElement signInButtonHomePage;

    @FindBy(how = How.ID, using = "txt_Username")
    private WebElement userName;

    @FindBy(how = How.ID, using = "txt_Password")
    private WebElement password;

    @FindBy(how = How.ID, using = "btn_SignIn")
    private WebElement signInButtonInner;

    @FindBy(how = How.ID, using = "btn_GenEbayToken")
    private WebElement linkMyAccountButton;

    public void signInButtonClickHomePage() {
        signInButtonHomePage.click();
    }

    public void enterUserName() {
        userName.sendKeys("cyberwolf_1");
    }

    public void enterPassword() {
        password.sendKeys("India99%%");
    }

    public void clickSignIn() {
        signInButtonInner.click();
    }

    public boolean linkMyAccount() {
        return linkMyAccountButton.isDisplayed();
    }

}
