package Selenium;

import CustomAnnotation.TestRailMapping;
import TestRail.TestResult;
import org.junit.Assert;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.support.PageFactory;
import org.testng.annotations.Test;

public class PageObjectAuctivaInstantiate {

    WebDriver driver = null;
    private static  String chromeDriverExecutablePath=".//chromedriver.exe";

    public boolean signInAuctiva() throws InterruptedException {
        PageObjectAuctiva pageObjectAuctiva = PageFactory.initElements(getWebDriver(), PageObjectAuctiva.class);
        Thread.sleep(10000);
        pageObjectAuctiva.signInButtonClickHomePage();
        Thread.sleep(5000);
        pageObjectAuctiva.enterUserName();
        pageObjectAuctiva.enterPassword();
        pageObjectAuctiva.clickSignIn();
        Thread.sleep(5000);
        return pageObjectAuctiva.linkMyAccount();
    }

    public WebDriver getWebDriver() {
        System.setProperty("webdriver.chrome.driver",chromeDriverExecutablePath);
        driver = new ChromeDriver();
        driver.get("https://www.auctiva.com/");
        return driver;
    }

    @Test
    @TestRailMapping(suiteName = "Auctiva Login",testName = "Login to Auctiva")
    public void validateSuccessFullLogin() throws Exception {
        TestResult testResult=new TestResult();
        Assert.assertTrue("Login is successFull", signInAuctiva());
        driver.quit();
        testResult.populateTestResult("CYB_TRIAL_AUTOMATION","","Login to Auctiva","Auctiva Login-Run");
    }
}
