package CustomAnnotation;


import Selenium.PageObjectAuctivaInstantiate;
import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;
import java.util.HashMap;
import java.util.Map;

public class ReadAnnotationValues {

    public Map<String,String> getAnnotationValues(Class<?> clazz)  {
        Map<String,String>map = new HashMap<String, String>();
        for (Method method : clazz.getDeclaredMethods()) {
            if (method.isAnnotationPresent(TestRailMapping.class)) {
                method.setAccessible(true);
                TestRailMapping annotation = method.getAnnotation(TestRailMapping.class);
                map.put(annotation.suiteName(),annotation.testName());

            }
        }
        return map;
    }

    public static void main(String[] args) throws InvocationTargetException, IllegalAccessException {
        ReadAnnotationValues readAnnotationValues = new ReadAnnotationValues();
        System.out.println(readAnnotationValues.getAnnotationValues(PageObjectAuctivaInstantiate.class));
    }
}
